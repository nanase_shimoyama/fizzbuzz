/**
 * 
 */
package testCheckFizzBuzz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jp.co.fizzbuzz.FizzBuzz;

/**
 * @author shimoyama.nanase
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
	/**
	 * {@link FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */
	@Test
	public void testCheckFizzBuzzTest() {
		
	//引数に9を渡した場合	「Fizz」が取得できる
	assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	
	//引数に20を渡した場合	「Buzz」が取得できる
	assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	
	//引数に45を渡した場合	「FizzBuzz」が取得できる
	assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	
	//引数に44を渡した場合	「44」が取得できる
	assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	
	//引数に46を渡した場合	「46」が取得できる
	assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
}

	